# GitLab CI templates

Files in this repository are provided as a courtesy by the TPA team to
help you build CI pipelines. This README file gives a summary
documentation of each template, linking to more in-depth documentation
in the TPA wiki as well.

In general, a template should be included like this:

    include:
      project: tpo/tpa/ci-templates
      file: .gitlab-ci.yml

## Static mirror shim deployments

The [static shim service](https://gitlab.torproject.org/tpo/tpa/team/-/wikis/service/static-shim) requires extensive logic that is
encapsulated in the `static-shim-deploy.yml` file, which can be
included like this:

    include:
      project: tpo/tpa/ci-templates
      file: static-shim-deploy.yml

Make sure you provide the `STATIC_GITLAB_SHIM_SSH_PRIVATE_KEY` `File`
variables in the project's CI/CD settings. That variable is a *secret*
and should absolutely *not* be set in the `.gitlab-ci.yml` file.

You can set the `SUBDIR` variable (in `.gitlab-ci.yml` or the project
variables) if it is something different than the default (`public/`).

The `SITE_URL` and `STAGING_URL` variables are used to configure
deployment environments in GitLab CI. When the `deploy-prod` and
`deploy-staging` run, GitLab creates new deployments that can be
examined from Deployments -> Environments.

Finally, the `*_URL` variables are also used to avoid running multiple
deploys in parallel for the same site. You will want to set it for your
pipelines to avoid concurrency with other, unrelated sites.

See the [static shim documentation](https://gitlab.torproject.org/tpo/tpa/team/-/wikis/service/static-shim#deploying-a-static-site-from-gitlab-ci) for complete documentation.

## Linting

The templates here are automatically linted with a script that takes
an API token from the @tpa-validation-bot user, which is stored in
this project's CI/CD variables.
